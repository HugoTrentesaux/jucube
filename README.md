# Jucube

Jucube, pour **Ju**pyter-**Ju**lia-**Ju**ne, est un tutoriel d'analyse de données sur l'exemple de la toile de confiance Ğ1. Le résultat de l'exécution est lisible dans GitLab, mais il est plus instructif d'exécuter les cellules chez soi à l'aide de l'outil Jupyter Notebook.

## Installation

### Julia

Installer `julia` (https://julialang.org/) par la méthode de votre choix

### Jupyter

Installer `jupyterlab` (https://jupyter.org/install) par la méthode de votre choix.

Exemple via une installation existante de conda/mamba `mamba install jupyterlab`

Exemple vie le gestionnaire de paquet julia : `pkg> add Conda`

### Jucube

- cloner le dépôt `git clone https://git.duniter.org/HugoTrentesaux/jucube.git`
- démarrer une REPL Julia dans le projet avec `julia --project=./`
- installer les dépendances `pkg> instantiate`, (`]` pour passer en mode package)
- démarrer le notebook `using IJulia; notebook(dir="./")`


